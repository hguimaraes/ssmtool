import numpy as np
import matplotlib.pyplot as plt

def plotfield(field_x, field_y, newfilename):
        """
        plotfield method:
            Function to plot a graph of "field_x" by "field_y" and save the image associated with
            "newfilename" name.

        Input :
            @param field_x [(double) (double) ...] List with the values to plot in the X axis
            @param field_y [(double) (double) ...] List with the values to plot in the Y axis
            @param newfilename String with the name of file to save the image result.

        Output :
            Create and save an image file with the name and extension specified by the user.
        
        ---
        """
        if len(field_x) == len(field_y):
            size = len(field_x)
        else:
            raise ValueError("The fields are not of the same size!")
        
        colors = np.random.rand(size)

        plt.scatter(field_x, field_y, c=colors, alpha=0.5)
        plt.grid(True)
        plt.savefig(newfilename)
        plt.clf()

def plot_as_table(surveytype, fields):
    """
    """
