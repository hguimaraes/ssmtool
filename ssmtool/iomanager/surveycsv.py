#!/usr/bin/python
"""
@Survey_CSV Class:
     It is a class to implement useful methods to manipulate CSV file of an survey
    in an easy way. It's designed to return python list which can be
    integrated and parallized with the Apache Spark framework. 

@Dependencies:
    * numpy
    * csv
    * MATPLOTLIB
---
"""

import csv
import numpy as np

class Survey_CSV(object):
    def __init__(self, filename, delimiter):
        """ 
        Survey_CSV Constructor :
            Constructor of the Class and method to open the csv file of surveys.
        Input: 
            @param filename (String) Path to the csv file.
            @param delimiter (String) Delimiter of csv fields.
        ---
        """
        self.filename = filename
        self.delimiter = delimiter
        # Open the CSV file as a python list
        with open(self.filename,'rb') as csvfile:
            self.hdulist = csv.reader(csvfile,delimiter = self.delimiter)
            self.hdulist = list(self.hdulist)

    def __del__(self):
        """
        Survey_CSV Destructor : 
            Function to close the csv file and destroy the storaged data.
        ---
        """
        del self.hdulist

    def getsurveycsv(self):
        """
        getsurveycsv method:
            Function to return the list associated with the csv data.
        Output:
            A python list with the data associated with the csv file.

        ---
        """
        return np.array(self.hdulist)

    def getsurveyheader(self):
        """
        getsurveyheader method:
            Function to return the header associated to the csv file.

        Output:
            A python list with the names of the headers in the current csv file.
        ---
        """
        return self.hdulist[0]

    def getsurveydata(self, rownumber, colnumber):
        """
        getsurveydata method:
            Function to return a single value (Any type) from a cell in the csv file.

        Input:
            @param rownumber (int) Index of the Row of the data.
            @param colnumber (int) Index of the Collumn of the data.

        Output:
            A value (of any type) corresponding to the hdulist[rownumber][colnumber]
            
        ---    
        """
        try:
            return np.array(float(self.hdulist[rownumber+1][colnumber]))
        except:
            return np.array(self.hdulist[rownumber+1][colnumber])

    def getsurveyrow(self, linenumber):
        """
        getsurveyrow method:
             Function to extract an entire row from the csv file. This function returns a
            python list.

        Input:
            @param linenumber (int) Number of the line to extract the entire row data

        Output:
            A python list with values of a specific row of the csv file.
        ---    
        """
        try:
            return np.array(map(float, self.hdulist[linenumber+1]))
        except:
            return np.array(self.hdulist[linenumber+1])

    def getsurveycoln(self, fieldname):
        """
        getsurveycoln method:
             Function to return an entire column from the csv file. The function returns
            a python list.

        Input:
            @param fieldname (string) name of the field to be extracted.

        Output:
            A python list with the values of a specific field (or column) of the csv file. 
        ---    
        """
        try:
            coln = self.hdulist[0].index(fieldname)

            hducoln = []
            for x in range(1, len(self.hdulist)):
                hducoln.append(self.hdulist[x][coln])
            try:
                return np.array(map(float,hducoln))
            except:
                return np.array(hducoln)
        except:
            print "The field doesn't exist! Use getsurveyheader to list all fields that you can use."