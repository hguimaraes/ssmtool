#!/usr/bin/python
"""
@Survey_FITS Class:
     It is a class to implement useful methods to manipulate FITS file
    in an easy way. It's designed to return python list which can be
    integrated and parallized with the Apache Spark framework. 

@Dependencies:
    * numpy
    * Astropy
    * MATPLOTLIB
---
"""

import numpy as np
from astropy.io import fits

class Survey_FITS(object):
    def __init__(self, filename):
        """ 
        Survey_FITS Constructor :
            Constructor of the Class and method to open the FITS file.
        Input: 
            @param filename (String) Path to the FITS file.
        ---
        """
        self.filename = filename
        self.hdulist = fits.open(filename, ignore_missing_end = True)[1].data

    def __del__(self):
        """
        Survey_FITS Destructor : 
            Function to close the FITS file and destroy the storaged data.
        ---
        """
        del self.hdulist

    def getsurveyfits(self):
        """
        getsurveyfits method:
            Function to return the FITS data in memory as a python list.
        Output:
            A python list with the data associated with the FITS file.

        *hint : You will need the Astropy package to handle this type of data.        
        ---
        """
        return self.hdulist

    def writesurveyfits(self, newfilename):
        """
        writesurveyfits method:
            Function to write a new FITS file if the user modify the current file.
        Input:
            @newfilename (string) Path to write the new FITS file.
        ---    
        """
        self.hdulist.writeto(newfilename)

    def getsurveyheader(self):
        """
        getsurveyheader method:
            Function to return the header associated to the FITS file.

        Output:
            A python list with the names of the headers in the current FITS file.
        ---
        """
        return self.hdulist.columns.names

    def getsurveydata(self, rownumber, colnumber):
        """
        getsurveydata method:
            Function to return a single value (Any type) from a cell in the FIT file.

        Input:
            @param rownumber (int) Index of the Row of the data.
            @param colnumber (int) Index of the Collumn of the data.

        Output:
            A value (of any type) corresponding to the hdulist[rownumber][colnumber]
            
        ---    
        """
        return self.hdulist[rownumber][colnumber]

    def getsurveyrow(self, linenumber):
        """
        getsurveyrow method:
             Function to extract an entire row from the FIT file. This function returns a
            python list.

        Input:
            @param linenumber (int) Number of the line to extract the entire row data

        Output:
            A python list with values of a specific row of the FITS file.
        ---    
        """
        return self.hdulist[linenumber]

    def getsurveycoln(self, fieldname):
        """
        getsurveycoln method:
             Function to return an entire column from the FIT file. The function returns
            a python list.

        Input:
            @param fieldname (string) name of the field to be extracted.

        Output:
            A python list with the values of a specific field (or column) of the FITS file. 
        ---    
        """
        return self.hdulist.field(fieldname)