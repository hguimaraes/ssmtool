#!/usr/bin/python
"""
@idmatching function:
	  It is a function to make the match between two list
	using simple python functions. Return a list with the value
	and the Index where the find this value in the Match List.
	If the Index is 'None' implies that value isn't in the match 
	list.

@Sparkidmatching function:
	  It is a function to make the match between two RDDs pairs (key-value)
	using a spark transformation. This transformation is based on a
	SQL operation (Join). Return a RDD with the same size of the reference 
	RDD and consists of the original key and the value associated with this
	key in the Reference RDD and the value associated with this key in the
	Match RDD. If the Output is of the type : [key, (value, 'None')] means
	that key isn't in the match RDD.

@Dependencies:
	* numpy
	* PySpark
---
"""

import numpy as np
from pyspark import SparkContext, SparkConf

def idmatching(cat_A_unicID, cat_B_unicID):
	"""
	idmatching function :
		Function to make the Index Matching in a sequential form.
	
	Input :
		@param cat_A_unicID A python list with the unique IDs from the reference Catalog.
		@param cat_B_unicID A python list with the unique IDs from the Matching Catalog.

	Output :
		@param idList(Value, KeyID) A python list with the same size of the reference catalog and a tuple with
			value and the id where you can find that value. 
	"""
	def match(posA, coordA, catalogB):
		"""
		match function :
			Nested function to perform the id matching. It's a linear search on a list.
		"""
		try:
			return (coordA, (posA, catalogB.index(coordA)))
		except:
			return (coordA, (posA, None))

	idList = []
	cat_A_unicID = list(cat_A_unicID)
	cat_B_unicID = list(cat_B_unicID)

	for x in range(len(cat_A_unicID)):
		idList.append(match(x, cat_A_unicID[x], cat_B_unicID))

	return idList

def Sparkidmatching(RDD_cat_A_unicID, RDD_cat_B_unicID):
	"""
	idmatching function :
		Function to make the Index Matching in a parallel form using Apache Spark.
	
	Input :
		@param RDD_cat_A_unicID A RDD with tuples of unique IDs from the reference Catalog
		and a value that represents the position of that key in a python list. [(Key,Value) ...]

		@param RDD_cat_B_unicID A RDD with tuples of unique IDs from the reference Catalog
		and a value that represents the position of that key in a python list. [(Key,Value) ...]

	Output :
		[(Key, (ReferenceValue, MatchValue)) ...] A RDD with each key of the Reference List, the index
		associated with this key and the index associated with that key in the Match RDD. If the Index 
		of the match RDD is none means the key doesn't exist in the match catalog. 
	"""
	return RDD_cat_A_unicID.leftOuterJoin(RDD_cat_B_unicID)