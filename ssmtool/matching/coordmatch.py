#!/usr/bin/python
"""
"""

import numpy as np
from esutil import htm
from astropy import units as u
from astropy.coordinates import SkyCoord
from pyspark import SparkContext, SparkConf

def kdt_celestial_matching(cat_A_ra_dec, cat_B_ra_dec, radius,
    exclude_multiple = False, n_neigh = 1):
    """
    """
    print "Performing the match on the celestial sphere between two catalogs..."
    print "Matching type: Using K-Dimensional Trees improved version from astropy..."

    cat_A_ra, cat_A_dec = zip(*cat_A_ra_dec)
    cat_B_ra, cat_B_dec = zip(*cat_B_ra_dec)

    print "Number of objects in catalog A: %s" % str(len(cat_A_ra))
    print "Number of objects in catalog B: %s" % str(len(cat_B_ra))

    c = SkyCoord(ra = cat_A_ra*u.degree, dec = cat_A_dec*u.degree)  
    catalog = SkyCoord(ra = cat_B_ra*u.degree, dec = cat_B_dec*u.degree)  
    idx, d2d, d3d = c.match_to_catalog_sky(catalog)

    return idx, d2d, d3d

def htm_celestial_matching(cat_A_ra_dec, cat_B_ra_dec, radius,
    exclude_multiple = False, n_neigh = 1, depth = 10):
    
    print "Performing the match on the celestial sphere between two catalogs..."
    print "Matching type: Using Hierarchical Triangular Mesh version from esutil..."

    h = htm.HTM(depth)

    cat_A_ra, cat_A_dec = zip(*cat_A_ra_dec)
    cat_B_ra, cat_B_dec = zip(*cat_B_ra_dec)

    print "Number of objects in catalog A: %s" % str(len(cat_A_ra))
    print "Number of objects in catalog B: %s" % str(len(cat_B_ra))

    if exclude_multiple:
       # Exclude objects with multiple matching
       m1,m2,d12 = h.match(cat_A_ra, cat_A_dec, cat_B_ra, cat_B_dec, radius,
                            maxmatch = 0)

        mltp = (np.arange(len(m2)) < len(m2))

        for i in range(len(m2)):
            if (m2[i] == m2).sum() > 1:
                mltp = ~(m2[i] == m2)&mltp
            else: pass
    
        m1 = m1[mltp]
        m2 = m2[mltp]
        d12 = d12[mltp]

    else: 
        m1,m2,d12 = h.match(cat_A_ra, cat_A_dec, cat_B_ra, cat_B_dec, radius, 
                            maxmatch = n_neigh)

    print "Number of objects in final matched catalogs: %s" % str(len(m2))
    return m1, m2, d12

def sparkcelestial_matching(RDD_cat_A_ra_dec, RDD_cat_B_ra_dec, radius,
    exclude_multiple = False, n_neigh = 1):
    """
    """ 
    return