"""
ID Matching :
	* Objective: 
		  This example shows how to do a match based on
		the ID of two catalogs: Um reference catalog and
		another to match. It suposes the reference catalog
		was generate from the match catalog and now we want
		to discover where the itens of the reference catalog
		are in the original (To import more columns or something else).

		  Given two lists with tuples (key,value) we are going to
		check if the value of the reference list is on the match list. 
		The key field is generated to keep the information about position
		when we transform the list into a RDD.

		  In the ssmtool are two functions implemented: One to deal with the 
		problem using a sequential approach and another using Spark.As output
		of these functions we expected a list with the value of the reference list
		and the index of this value in the match list, to the programmer operate on it.
	
	* How to use:
		./pyspark idmathcing.py
		(Before apply this check the files to understand the result)	
"""
import sys
import numpy as np
from astropy.io import fits
from pyspark import SparkConf, SparkContext
from ssmtool.iomanager import surveycsv as scsv
from ssmtool.iomanager import surveyfits as sf
from ssmtool.matching import idmatch as match

def main(argv):
	# Set the configuration of the Spark Application
	conf = (SparkConf().setMaster("local[*]").setAppName("IDMatching"))

	# Creating a Spark context with the previous configuration
	sc = SparkContext(conf = conf)
	
 	# Loading the data
 	##### Reference catalog
 	CSVfilename = "data/HubbleRef.csv"
 	csv_ref_survey = scsv.Survey_CSV(CSVfilename,',')
 	uniqID_ref_survey = csv_ref_survey.getsurveycoln('NUMBER')

 	##### Catalog to match
 	FITSfilename = "data/HubbleMatch.fit"
 	fit_match_survey = sf.Survey_FITS(FITSfilename)
 	uniqID_match_survey = fit_match_survey.getsurveycoln('NUMBER')

 	# Apply ID Matching in Lists
 	print uniqID_ref_survey
 	print uniqID_match_survey

 	listIDIndex = match.idmatching(uniqID_ref_survey,uniqID_match_survey);
 	print sorted(listIDIndex)

 	# Transform into a Spark RDD
 	##### Creating Tuples of elements and id
 	uniqID_ref_survey   = zip(uniqID_ref_survey, range(len(uniqID_ref_survey)))
 	uniqID_match_survey = zip(uniqID_match_survey, range(len(uniqID_match_survey)))

 	RDDRefSurvey   = sc.parallelize(uniqID_ref_survey) 
 	RDDMatchSurvey = sc.parallelize(uniqID_match_survey)

 	# Apply ID Matching in RDDs
 	RDD_matchResult = match.Sparkidmatching(RDDRefSurvey,RDDMatchSurvey).collect()
 	print sorted(RDD_matchResult)

if __name__ == "__main__":
 	main(sys.argv)