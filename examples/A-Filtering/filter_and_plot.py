"""
Plotting as Graph :
	* Objective: 
		  This program example shows how to filter data
		from a fit file and plot the result. The data is
		filtered using Apache Spark.

		  Given a list with tuples (x,y) we are going to
		check if the value of Y it's more different then 
		10 percent of the X value. MAG_ISO and MAG_AUTO 
		represent the bright of a galaxy or a star and 
		should have the same value.

		We expect as a result a graph of a Y = X (or close).
	* How to use:
		 ./pyspark filter_and_plot.py 10
		 ( To filter when MAG_AUTO x MAG_ISO have a relative 
		  deviation higher then 10 percent.)	
"""
import sys
from pyspark import SparkConf, SparkContext
from ssmtool.iomanager import plotdata
from ssmtool.iomanager import surveyfits as sf

def main(argv):
	# Set the configuration of the Spark Application
	conf = (SparkConf().setMaster("local[*]").setAppName("FilteringAndPlot"))

	# Creating a Spark context with the previous configuration
	sc = SparkContext(conf = conf)
	
	# Loading the data
	filename = "data/u5at0401b_3.fit"
	field_x_name = "MAG_ISO"
	field_y_name = "MAG_AUTO"

	try:
		filterValue = float(sys.argv[1]) / 100
	except:
		filterValue = 0.1

	# Name of the Fields we want plot and extract these field from the fit file
	fitsurvey = sf.Survey_FITS(filename)
	fieldX = fitsurvey.getsurveycoln(field_x_name) 
	fieldY = fitsurvey.getsurveycoln(field_y_name)
	
	# Saving a plot of the original fields
	print "Number of objects before the filter: %s" % str(len(fieldX))
	plotdata.plotfield(fieldX, fieldY,"result/OriginalFields.png")

	#  Filtering the data and applying some statistics metrics
	# We are operating over a lists extracted from the fit file.
	# The original file remains the same.
	cfields = zip(fieldX,fieldY)
	rdd = sc.parallelize(cfields).filter(lambda (x,y): abs((y-x)/x) <= filterValue).collect()
	
	# Ploting the results from filter phase
	rdd_field_x,rdd_field_y = zip(*rdd)
	print "Number of objects after the filter: %s" % str(len(rdd_field_x))
	plotdata.plotfield(rdd_field_x, rdd_field_y,"result/FilteredField.png")
	
if __name__ == "__main__":
	main(sys.argv)