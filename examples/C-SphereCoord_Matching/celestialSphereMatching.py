"""
"""
import sys
from pyspark import SparkConf, SparkContext
from ssmtool.iomanager import surveycsv as scsv
from ssmtool.iomanager import surveyfits as sf
from ssmtool.matching import coordmatch as match

def main(argv):
	# Set the configuration of the Spark Application
	conf = (SparkConf().setMaster("local[*]").setAppName("SphericalCoordMatching"))
	
	# Creating a Spark context with the previous configuration
	sc = SparkContext(conf = conf)

if __name__ == "__main__":
	main(sys.argv)

