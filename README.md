## ***SSMTool : Spark Survey Matching Tool***

### What is the SSMTool?
SSMTool is a python module to work with Astrophysics and Apache Spark. It has a set of classes and functions to handle more easily fits file and csv. Most part of the methods handling files return a python list wich can be parallelized without so much effort using the Apache Spark framework.


### Objectives :
* Handle FITS and CSV files with Catalogues and Surveys.
* Matching using ID and Matching in Spherical Coordinates.

### Examples included :
* A - Plotting the result of a filter in a FITS catalog (Hubble/NASA data).
* B - Matching a CSV catalog and a FITS catalog using ID (CStripe 82 data). 
* C - Double Matching of Galaxies/Stars in the Celestial Sphere (CStripe 82 and VICS82 data).

### Dependencies

Package | Version
------- | -------
[Apache Spark](https://spark.apache.org/) | 1.3.0
[Astropy](http://www.astropy.org/) | 1.0.2
[esutil](https://github.com/esheldon/esutil) | 0.5.2
[matplotlib](http://matplotlib.org/) | 1.4.2
[NumPy](http://www.numpy.org/) | 1.9.2

* Tip: To visualize FITS files quickly, use the NASA program [fv](http://heasarc.gsfc.nasa.gov/ftools/fv/fv_download.html).

#### How to install the dependencies
* Debian based systems
```shell-script
    sudo apt-get install python-pip
    sudo pip install --upgrade pip
    sudo pip install numpy matplotlib astropy esutil 
```
* RedHat based systems
```shell-script
    sudo yum install epel-release
    sudo yum -y install  python-pip
    sudo pip install --upgrade pip
    sudo pip install numpy matplotlib astropy esutil
``` 
### Acknowledgement
* Clécio R. De Bom - ICRA/CBPF
* Martin Mackler - ICRA/CBPF

### Authors
* Heitor R. Guimarães